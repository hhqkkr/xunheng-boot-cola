package com.xunheng.adapter.system.web;

import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.MatrixService;
import com.xunheng.client.system.dto.MatrixSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.client.system.dto.query.MatrixListQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="矩阵")
@RestResponse
@RequestMapping("/xunheng-boot/matrix")
public class MatrixController {

    @Resource
    MatrixService matrixService;

    @PermissionCode("none")
    @Log(module = "矩阵",title = "矩阵所有列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    @Operation(summary = "矩阵所有列表")
    public List<MatrixVO> getAll(MatrixListQuery query){
        return matrixService.getAll(query);
    }

    @PermissionCode("none")
    @Log(module = "矩阵",title = "矩阵新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "矩阵新增")
    public MatrixVO save(@RequestBody MatrixSaveCmd cmd){
        return matrixService.saveOrUpdate(cmd);
    }

    @PermissionCode("none")
    @Log(module = "矩阵",title = "矩阵修改", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "矩阵修改")
    public MatrixVO edit(@RequestBody MatrixSaveCmd cmd){
        return matrixService.saveOrUpdate(cmd);
    }

    @PermissionCode("none")
    @Log(module = "矩阵",title = "矩阵批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "矩阵批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id : ids){
            matrixService.remove(id);
        }
        return "删除成功";
    }

}
