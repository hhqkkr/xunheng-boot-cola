package com.xunheng.adapter.system.web;

import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.MatrixDataService;
import com.xunheng.client.system.dto.MatrixDataSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.client.system.dto.query.MatrixResultByConditionQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="矩阵数据")
@RestResponse
@RequestMapping("/xunheng-boot/matrixData")
public class MatrixDataController {

    @Resource
    MatrixDataService matrixDataService;

    @PermissionCode("none")
    @RequestMapping(value = "/getByMatrix/{id}",method = RequestMethod.GET)
    @Operation(summary = "矩阵数据获取")
    public MatrixVO getConfigDataByMatrix(@PathVariable String id){
        return matrixDataService.getConfigDataByMatrix(id);
    }

    @PermissionCode("none")
    @Log(module = "矩阵数据",title = "矩阵数据新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "矩阵数据新增")
    public MatrixVO save(@RequestBody MatrixDataSaveCmd cmd){
        return matrixDataService.saveMatrixData(cmd);
    }

    @PermissionCode("none")
    @RequestMapping(value = "/getResultByCondition",method = RequestMethod.GET)
    @Operation(summary = "查询矩阵匹配数据")
    public List<String> getResultByCondition(MatrixResultByConditionQuery query){
        return matrixDataService.getResultByCondition(query);
    }
}
