package com.xunheng.adapter.system.web;

import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.client.system.api.DictService;
import com.xunheng.client.system.dto.DictSaveCmd;
import com.xunheng.client.system.dto.VO.DictVO;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.domain.core.log.annotation.Log;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="字典")
@RestResponse
@RequestMapping("/xunheng-boot/dict")
public class DictController {

    @Resource
    DictService dictService;

    @PermissionCode("system.dict.allList")
    @Log(module = "字典",title = "字典分页列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    @Operation(summary = "字典全部列表")
    public List<DictVO> getAll(){
        return dictService.getAll();
    }

    @PermissionCode("system.dict.add")
    @Log(module = "字典",title = "字典新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @Operation(summary = "字典新增")
    public DictVO add(@RequestBody @Validated DictSaveCmd cmd){
        return dictService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.dict.edit")
    @Log(module = "字典",title = "字典编辑", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "字典编辑")
    public DictVO edit(@RequestBody @Validated DictSaveCmd cmd){
        return dictService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.dict.delete")
    @Log(module = "字典",title = "字典删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{id}",method = RequestMethod.DELETE)
    @Operation(summary = "字典删除")
    public String remove(@PathVariable String id){
        dictService.remove(id);
        return "删除成功";
    }

}
