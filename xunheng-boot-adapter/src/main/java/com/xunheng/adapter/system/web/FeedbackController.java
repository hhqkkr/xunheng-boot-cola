package com.xunheng.adapter.system.web;

import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.FeedbackService;
import com.xunheng.client.system.dto.FeedbackSaveCmd;
import com.xunheng.client.system.dto.VO.FeedbackVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Tag(name="意见反馈")
@RestResponse
@RequestMapping("/xunheng-boot/feedback")
public class FeedbackController {

    @Resource
    FeedbackService feedbackService;

    @PermissionCode("none")
    @Log(module = "意见反馈",title = "保存意见反馈", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "保存意见反馈")
    public FeedbackVO save(@RequestBody FeedbackSaveCmd cmd){
        return feedbackService.saveOrUpdate(cmd);
    }

}
