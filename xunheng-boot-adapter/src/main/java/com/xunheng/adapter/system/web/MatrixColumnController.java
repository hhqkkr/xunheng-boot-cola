package com.xunheng.adapter.system.web;

import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.MatrixColumnService;
import com.xunheng.client.system.dto.MatrixColumnSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Tag(name="矩阵列")
@RestResponse
@RequestMapping("/xunheng-boot/matrixColumn")
public class MatrixColumnController {

    @Resource
    MatrixColumnService matrixColumnService;

    @PermissionCode("none")
    @RequestMapping(value = "/getByMatrix/{id}",method = RequestMethod.GET)
    @Operation(summary = "矩阵列配置获取")
    public MatrixVO getByMatrix(@PathVariable String id){
        return matrixColumnService.getColumnByMatrix(id);
    }

    @PermissionCode("none")
    @Log(module = "矩阵列",title = "矩阵列保存", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "矩阵列新增")
    public MatrixVO save(@RequestBody MatrixColumnSaveCmd cmd){
        return matrixColumnService.saveMatrixColumn(cmd);
    }

}
