package com.xunheng.adapter.system.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.TenantService;
import com.xunheng.client.system.dto.TenantSaveCmd;
import com.xunheng.client.system.dto.VO.TenantVO;
import com.xunheng.client.system.dto.query.TenantPageQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Tag(name="租户")
@RestResponse
@RequestMapping("/xunheng-boot/tenant")
public class TenantController {

    @Resource
    TenantService tenantService;

    @PermissionCode("system.tenant.pageList")
    @Log(module = "租户",title = "租户分页列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getPageList",method = RequestMethod.GET)
    @Operation(summary = "租户分页列表")
    public IPage<TenantVO> getPageList(TenantPageQuery query){
        return tenantService.pageList(query);
    }

    @PermissionCode("system.tenant.detail")
    @Log(module = "租户",title = "租户详情", businessType = BusinessType.DETAIL)
    @RequestMapping(value = "/getOne/{id}",method = RequestMethod.GET)
    @Operation(summary = "租户详情")
    public TenantVO getOne(@PathVariable String id){
        return tenantService.getDetail(id);
    }

    @PermissionCode("system.tenant.add")
    @Log(module = "租户",title = "租户新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "租户新增")
    public TenantVO save(@RequestBody @Validated TenantSaveCmd cmd){
        return tenantService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.tenant.edit")
    @Log(module = "租户",title = "租户修改", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "租户修改")
    public TenantVO edit(@RequestBody @Validated TenantSaveCmd cmd){
        return tenantService.saveOrUpdate(cmd);
    }


    @PermissionCode("system.tenant.deleteBatch")
    @Log(module = "租户",title = "租户批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "租户批量删除")
    public String delByIds(@PathVariable String[] ids){
       for(String id : ids){ tenantService.remove(id);}
       return "删除成功！";
    }

}
