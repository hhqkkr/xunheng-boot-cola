package com.xunheng.adapter.system.web;

import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.OrganizationService;
import com.xunheng.client.system.dto.OrganizationCreateCmd;
import com.xunheng.client.system.dto.OrganizationUpdateCmd;
import com.xunheng.client.system.dto.VO.OrganizationVO;
import com.xunheng.client.system.dto.query.OrganizationListQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="组织架构")
@RestResponse
@RequestMapping("/xunheng-boot/organization")
public class OrganizationController {

    @Resource
    OrganizationService organizationService;

    @PermissionCode("system.organization.tree")
    @Log(module = "组织架构",title = "组织架构树", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getList", method = RequestMethod.GET)
    @Operation(summary = "组织架构树")
    public List<OrganizationVO> getList(OrganizationListQuery query){
        return organizationService.getGroupOrganizationList(query);
    }

    @PermissionCode("system.organization.add")
    @Log(module = "组织架构",title = "组织架构新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @Operation(summary = "组织架构新增")
    public OrganizationVO add(@RequestBody @Validated OrganizationCreateCmd cmd){
        return organizationService.create(cmd);
    }

    @PermissionCode("system.organization.edit")
    @Log(module = "组织架构",title = "修改组织架构", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "编辑")
    public OrganizationVO edit(@RequestBody OrganizationUpdateCmd cmd){
        return organizationService.update(cmd);
    }

    @PermissionCode("system.organization.deleteBatch")
    @Log(module = "组织架构",title = "组织架构批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "组织架构批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id : ids){
            organizationService.remove(id);
        }
        return "删除成功";
    }

}
