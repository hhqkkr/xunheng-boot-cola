package com.xunheng.adapter.system.web;

import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.client.system.api.CustomConfigService;
import com.xunheng.client.system.dto.PersonalConfigSaveCmd;
import com.xunheng.client.system.dto.SystemConfigSaveCmd;
import com.xunheng.client.system.dto.VO.CustomConfigVO;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.domain.core.log.annotation.Log;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="自定义设置")
@RestResponse
@RequestMapping("/xunheng-boot/customConfig")
public class CustomConfigController {

    @Resource
    CustomConfigService customConfigService;

    @PermissionCode("none")
    @Log(module = "自定义设置",title = "保存个性化设置", businessType = BusinessType.ADD)
    @RequestMapping(value = "/savePersonalConfig",method = RequestMethod.POST)
    @Operation(summary = "保存个性化设置")
    public CustomConfigVO savePersonalConfig(@RequestBody @Validated PersonalConfigSaveCmd cmd){
        return customConfigService.savePersonalConfig(cmd);
    }

    @PermissionCode("none")
    @Log(module = "自定义设置",title = "保存系统拓展设置", businessType = BusinessType.ADD)
    @RequestMapping(value = "/saveExtendConfig",method = RequestMethod.POST)
    @Operation(summary = "保存系统拓展设置")
    public CustomConfigVO saveSystemConfig(@RequestBody @Validated SystemConfigSaveCmd cmd){
        return customConfigService.saveSystemConfig(cmd);
    }

    @PermissionCode("none")
    @Log(module = "自定义设置",title = "获取系统拓展配置", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getExtendConfigList",method = RequestMethod.GET)
    @Operation(summary = "获取系统拓展配置")
    public List<CustomConfigVO> getSystemConfigList(){
        return customConfigService.getSystemConfigList();
    }

    @PermissionCode("none")
    @Log(module = "自定义设置",title = "自定义设置批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "自定义设置批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id : ids){
            customConfigService.remove(id);
        }
        return "删除成功";
    }

}
