package com.xunheng.adapter.system.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.TenantAuthRecordService;
import com.xunheng.client.system.dto.TenantAuthRecordSaveCmd;
import com.xunheng.client.system.dto.VO.TenantAuthRecordVO;
import com.xunheng.client.system.dto.query.TenantAuthRecordPageQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Tag(name="租户授权记录")
@RestResponse
@RequestMapping("/xunheng-boot/tenantAuthRecord")
public class TenantAuthRecordController {

    @Resource
    TenantAuthRecordService tenantAuthRecordService;

    @PermissionCode("system.tenantAuthRecord.pageList")
    @Log(module = "租户授权记录",title = "租户授权记录分页", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getPageList",method = RequestMethod.GET)
    @Operation(summary = "租户授权记录分页")
    public IPage<TenantAuthRecordVO> getPageList(TenantAuthRecordPageQuery query){
        return tenantAuthRecordService.pageList(query);
    }

    @PermissionCode("system.tenantAuthRecord.detail")
    @Log(module = "租户授权记录",title = "租户授权记录详情", businessType = BusinessType.DETAIL)
    @RequestMapping(value = "/getOne/{id}",method = RequestMethod.GET)
    @Operation(summary = "租户授权记录详情")
    public TenantAuthRecordVO getOne(@PathVariable String id){
        return tenantAuthRecordService.getDetail(id);
    }


    @PermissionCode("system.tenantAuthRecord.add")
    @Log(module = "租户授权记录",title = "租户授权记录新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "租户授权记录新增")
    public TenantAuthRecordVO save(@RequestBody @Validated TenantAuthRecordSaveCmd cmd){
        return tenantAuthRecordService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.tenantAuthRecord.edit")
    @Log(module = "租户授权记录",title = "租户授权记录修改", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "租户授权记录修改")
    public TenantAuthRecordVO edit(@RequestBody @Validated TenantAuthRecordSaveCmd cmd){
        return tenantAuthRecordService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.tenantAuthRecord.deleteBatch")
    @Log(module = "租户授权记录",title = "租户授权记录批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "租户授权记录批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id : ids){
            tenantAuthRecordService.remove(id);
        }
        return "删除成功";
    }

}
