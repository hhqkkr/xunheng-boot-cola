package com.xunheng.adapter.system.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.client.system.dto.VO.RoleVO;
import com.xunheng.client.system.dto.VO.SelectItemVO;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.RoleService;
import com.xunheng.client.system.dto.RolePermEditCmd;
import com.xunheng.client.system.dto.RoleSaveCmd;
import com.xunheng.client.system.dto.query.RolePageQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="角色")
@RestResponse
@RequestMapping("/xunheng-boot/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @PermissionCode("none")
    @RequestMapping(value = "/getSelectItem", method = RequestMethod.GET)
    @Operation(summary = "获取选择框数据")
    public List<SelectItemVO> getSelectItem() {
        return roleService.getSelectItem();
    }

    @PermissionCode("system.role.pageList")
    @Log(module = "角色",title = "角色分页列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getPageList",method = RequestMethod.GET)
    @Operation(summary = "角色分页列表")
    public IPage<RoleVO> getPageList(RolePageQuery query){
        return roleService.pageList(query);
    }

    @PermissionCode("system.role.allList")
    @Log(module = "角色",title = "角色所有列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getAllList",method = RequestMethod.GET)
    @Operation(summary = "角色所有列表")
    public List<RoleVO> getAll(){
        return roleService.getAll();
    }

    @PermissionCode("system.role.editRolePerm")
    @Log(module = "角色",title = "角色分配权限", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/editRolePerm",method = RequestMethod.POST)
    @Operation(summary = "角色分配权限")
    public String editRolePerm(@RequestBody RolePermEditCmd cmd){
        roleService.editRolePerm(cmd);
        return "操作成功";
    }

    @PermissionCode("system.role.add")
    @Log(module = "角色",title = "角色新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "角色新增")
    public RoleVO save(@RequestBody @Validated RoleSaveCmd cmd){
        return roleService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.role.edit")
    @Log(module = "角色",title = "角色修改", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "角色修改")
    public RoleVO edit(@RequestBody @Validated RoleSaveCmd cmd){
        return roleService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.role.deleteBatch")
    @Log(module = "角色",title = "角色批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delAllByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "角色批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id:ids){
            roleService.remove(id);
        }
        return "删除成功";
    }

}
