package com.xunheng.adapter.system.web;

import com.xunheng.client.system.dto.VO.PermissionVO;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.PermissionService;
import com.xunheng.client.system.dto.PermissionCreateCmd;
import com.xunheng.client.system.dto.PermissionUpdateCmd;
import com.xunheng.client.system.dto.VO.UserPermissionVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Tag(name="权限菜单")
@RestResponse
@RequestMapping("/xunheng-boot/permission")
public class PermissionController {

    @Resource
    PermissionService permissionService;

    @RequestMapping(value = "/getUserPermission", method = RequestMethod.GET)
    @Operation(summary = "获取当前用户所有权限数据")
    public UserPermissionVO getUserPermission(){
        return permissionService.getUserPermission();
    }

    @PermissionCode("system.permission.tree")
    @Log(module = "权限菜单",title = "权限菜单树", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getAllList", method = RequestMethod.GET)
    @Operation(summary = "权限菜单树")
    public List<PermissionVO> getAllList(){
        return permissionService.getSystemPermissionList();
    }

    @PermissionCode("system.permission.add")
    @Log(module = "权限菜单",title = "权限菜单新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @Operation(summary = "权限菜单新增")
    public PermissionVO add(@RequestBody @Validated PermissionCreateCmd cmd){
        return permissionService.create(cmd);
    }

    @PermissionCode("system.permission.edit")
    @Log(module = "权限菜单",title = "权限菜单修改", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @Operation(summary = "权限菜单修改")
    public PermissionVO edit(@RequestBody @Validated PermissionUpdateCmd cmd){
        return permissionService.update(cmd);
    }

    @PermissionCode("system.permission.deleteBatch")
    @Log(module = "权限菜单",title = "权限菜单批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}", method = RequestMethod.DELETE)
    @Operation(summary = "权限菜单批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id:ids){
            permissionService.remove(id);
        }
        return "删除成功";
    }

}
