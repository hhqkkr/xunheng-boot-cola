package com.xunheng.adapter.system.web;

import com.xunheng.client.system.api.AuthService;
import com.xunheng.client.system.dto.AuthLoginCmd;
import com.xunheng.client.system.dto.VO.LoginVO;
import com.xunheng.domain.core.common.annotation.RestResponse;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


//"登录/登出"
@RestResponse
public class AuthController {

    @Resource
    private AuthService authService;

    @RequestMapping("/xunheng-boot/login")
    public LoginVO login(@RequestBody @Validated AuthLoginCmd cmd, HttpServletRequest request) {
        return authService.login(cmd,request);
    }

    @RequestMapping("/xunheng-boot/logout")
    public  String logout() {
        authService.logout();
        return "注销登录成功！";
    }

}
