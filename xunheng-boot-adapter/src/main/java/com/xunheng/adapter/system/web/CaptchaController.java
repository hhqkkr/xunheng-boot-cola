package com.xunheng.adapter.system.web;

import com.xunheng.client.system.api.CaptchaService;
import com.xunheng.client.system.dto.VO.CaptchaVO;
import com.xunheng.domain.core.common.annotation.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestResponse
@RequestMapping("/xunheng-boot/captcha")
public class CaptchaController {

    @Autowired
    CaptchaService captchaService;

    @RequestMapping(value = "/load",method = RequestMethod.GET)
    public CaptchaVO load(){
       return captchaService.loadCaptcha();
    }

}
