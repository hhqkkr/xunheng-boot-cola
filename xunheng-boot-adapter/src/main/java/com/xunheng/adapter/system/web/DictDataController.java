package com.xunheng.adapter.system.web;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.client.constant.CommonConstant;
import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.DictDataService;
import com.xunheng.client.system.dto.DictDataSaveCmd;
import com.xunheng.client.system.dto.VO.DictDataVO;
import com.xunheng.client.system.dto.query.DictDataPageQuery;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name="字典数据")
@RestResponse
@RequestMapping("/xunheng-boot/dictData")
public class DictDataController {

    @Resource
    DictDataService dictDataService;

    @PermissionCode("system.dictData.pageList")
    @Log(module = "字典数据",title = "字典数据分页列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getPageListByType",method = RequestMethod.GET)
    @Operation(summary = "字典数据分页列表")
    public IPage<DictDataVO> getPageListByType(DictDataPageQuery query){
        return dictDataService.pageList(query);
    }

    @PermissionCode("none")
    @RequestMapping(value = "/getByType",method = RequestMethod.GET)
    @Operation(summary = "字典数据所有列表")
    public List<DictDataVO> getByType(@RequestParam String type){
        return dictDataService.getByDictType(type);
    }

    @PermissionCode("system.dictData.add")
    @Log(module = "字典数据",title = "字典数据新增", businessType = BusinessType.ADD)
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @Operation(summary = "字典数据新增")
    public DictDataVO add(@RequestBody @Validated DictDataSaveCmd cmd){
        return dictDataService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.dictData.edit")
    @Log(module = "字典数据",title = "字典数据修改", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @Operation(summary = "字典数据修改")
    public DictDataVO edit(@RequestBody @Validated DictDataSaveCmd cmd){
        return dictDataService.saveOrUpdate(cmd);
    }

    @PermissionCode("system.dictData.disable")
    @Log(module = "字典数据",title = "字典数据禁用", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/disable/{dictDataId}",method = RequestMethod.POST)
    @Operation(summary = "字典数据禁用")
    @Parameters({
            @Parameter(name = "dictDataId",description = "字典数据id",in = ParameterIn.PATH),
    })
    public void disable(@PathVariable String dictDataId){
         dictDataService.updateDictStatus(dictDataId, CommonConstant.STATUS_DISABLE);
    }

    @PermissionCode("system.dictData.enable")
    @Log(module = "字典数据",title = "字典数据启用", businessType = BusinessType.EDIT)
    @RequestMapping(value = "/enable/{dictDataId}",method = RequestMethod.POST)
    @Operation(summary = "字典数据启用")
    @Parameters({
            @Parameter(name = "dictDataId",description = "字典数据id",in = ParameterIn.PATH),
    })
    public void enable( @PathVariable String dictDataId){
         dictDataService.updateDictStatus(dictDataId, CommonConstant.STATUS_NORMAL);
    }

    @PermissionCode("system.dictData.deleteBatch")
    @Log(module = "字典数据",title = "字典数据批量删除", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "字典数据批量删除")
    public String delByIds(@PathVariable String[] ids){
        for(String id : ids){
            dictDataService.remove(id);
        }
        return "批量通过id删除数据成功";
    }
}
