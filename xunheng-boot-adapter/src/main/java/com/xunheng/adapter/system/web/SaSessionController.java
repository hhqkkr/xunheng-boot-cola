package com.xunheng.adapter.system.web;

import com.xunheng.domain.core.common.annotation.PermissionCode;
import com.xunheng.domain.core.common.annotation.RestResponse;
import com.xunheng.client.log.enums.BusinessType;
import com.xunheng.domain.core.log.annotation.Log;
import com.xunheng.client.system.api.SaSessionService;
import com.xunheng.client.system.dto.VO.SaSessionVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Tag(name="session会话")
@RestResponse
@RequestMapping("/xunheng-boot/saSession")
public class SaSessionController {

    @Autowired
    SaSessionService saSessionService;

    @PermissionCode("system.saSession.pageList")
    @Log(module = "session会话",title = "session会话分页列表", businessType = BusinessType.LIST)
    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    @Operation(summary = "session会话分页列表")
    public List<SaSessionVO> getAll(@RequestParam(required = false) String username){
        return saSessionService.getAll(username);
    }

    @PermissionCode("system.saSession.kickOut")
    @Log(module = "session会话",title = "session会话下线", businessType = BusinessType.DELETE)
    @RequestMapping(value = "/kickOut/{token}",method = RequestMethod.DELETE)
    @Operation(summary = "session会话下线")
    public String kickOut(@PathVariable String token){
        saSessionService.kickOutByToken(token);
        return "下线用户成功";
    }

    @PermissionCode("system.saSession.changeSession")
    @Log(module = "session会话",title = "session会话切换身份", businessType = BusinessType.OTHER)
    @RequestMapping(value = "/changeSession/{token}",method = RequestMethod.POST)
    @Operation(summary = "session会话切换身份")
    public String changeSession(@PathVariable String token){
        saSessionService.changeSession(token);
        return "切换成功";
    }


}
