package com.xunheng.adapter.log.web;

import com.xunheng.client.log.api.SysLogService;
import com.xunheng.client.log.dto.LogCreateCmd;
import com.xunheng.client.log.dto.VO.LogVO;
import com.xunheng.client.log.dto.query.LogPageQuery;
import com.xunheng.domain.core.common.annotation.RestResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Tag(name="系统日志")
@RestResponse
@RequestMapping("/xunheng-boot/sysLog")
public class SysLogController {

    @Resource
    SysLogService sysLogService;

    @RequestMapping(value = "/getPageList",method = RequestMethod.GET)
    @Operation(summary = "系统日志分页列表")
    public Page<LogVO> getPageList(LogPageQuery query){
        return sysLogService.pageList(query);
    }

    @RequestMapping(value = "/getEchartsData",method = RequestMethod.GET)
    @Operation(summary = "系统日志echarts数据")
    public Map<String, Object> getEchartsData(){
        return sysLogService.getEchartsData();
    }

    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @Operation(summary = "系统日志批量删除")
    public String remove(@PathVariable String[] ids){
        for (String id : ids) {
            sysLogService.remove(id);
        }
        return "批量通过id删除数据成功";
    }

    @RequestMapping(value = "/clean",method = RequestMethod.DELETE)
    @Operation(summary = "系统日志清空")
    public String clean(){
        sysLogService.removeAll();
        return "系统日志清空成功";
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @Operation(summary = "保存数据")
    public LogVO save(@RequestBody LogCreateCmd cmd){
        return sysLogService.save(cmd);
    }

}
