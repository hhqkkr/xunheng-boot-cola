package com.xunheng.client.system.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class PersonalConfigSaveCmd {

    @NotBlank(message = "设置名称不能为空")
    @Schema(description = "设置名称")
    private String configKey;

    @NotBlank(message = "设置值不能为空")
    @Schema(description = "设置值")
    private String configValue;

}
