package com.xunheng.client.system.dto.VO;

import lombok.Data;

@Data
public class LoginVO {

    //用户信息VO
    private UserVO user;

   //用户token
    private String token;
}
