package com.xunheng.client.system.api;

import com.xunheng.client.system.dto.MatrixSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.client.system.dto.query.MatrixListQuery;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵信息service接口
 * @author: hhqkkr
 * @date: 2023/6/29 19:11
 */
public interface MatrixService {

    /**
     * 获取所有矩阵
     * @param query 查询dto
     * @return 矩阵信息列表
     */
    List<MatrixVO> getAll(MatrixListQuery query);

    /**
     * 保存或更新矩阵信息
     * @param cmd 保存操作dto
     * @return 矩阵信息
     */
    MatrixVO saveOrUpdate(MatrixSaveCmd cmd);

    /**
     * 删除矩阵信息
     * @param id 矩阵id
     */
    void remove(String id);
}
