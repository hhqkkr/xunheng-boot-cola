package com.xunheng.client.system.dto;

import com.xunheng.client.system.dto.VO.MatrixColumnVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class MatrixColumnSaveCmd {

    @Schema(description = "矩阵id")
    private String id;

    @Schema(description = "列字段列表")
    private List<MatrixColumnVO> columnList;

}
