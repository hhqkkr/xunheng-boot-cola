# xunheng-boot-cola(寻衡单体COLA架构后台管理框架)


## 介绍
寻衡boot是[寻衡cloud](https://gitee.com/hhqkkr/xunheng-cloud-cola)的单体版本，给需要单体的朋友使用，使用的**COLA4.0**架构配合**springboot3**+**satoken**+**mybatisplus**+**elasticsearch**+**tlog**等搭建的后台管理系统框架。只保留了日志与主要的后台系统功能服务

### 项目内容
关于项目的介绍，截图，cola架构的介绍等详细内容可以前往[cloud版本](https://gitee.com/hhqkkr/xunheng-cloud-cola)查看


### 开发环境与依赖版本
| java | springboot | mysql | mybatisplus   | cola |
|------|------------|-------|---------------|------|
| 17   | 3.0.2      | 5.7   | 3.5.4.1 | 4.3.2      | 

### 前端项目
前端项目依旧可以使用cloud的版本的前端项目[xunheng-cloud-cola-front](https://gitee.com/hhqkkr/xunheng-cloud-cola-front).基于scui开发，使用的elementplus+vue3

使用时修改后端的api的地址，并将api各服务的根地址修改为xunheng-boot

## 如果帮助到了你，可以给个小星星⭐️,共同进步。