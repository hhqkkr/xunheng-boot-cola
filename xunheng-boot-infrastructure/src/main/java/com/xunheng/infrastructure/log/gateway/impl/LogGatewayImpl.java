package com.xunheng.infrastructure.log.gateway.impl;

import com.xunheng.domain.log.LogEntity;
import com.xunheng.domain.log.LogGateway;
import com.xunheng.infrastructure.log.DO.EsLogDO;
import com.xunheng.infrastructure.log.convertor.LogConverter;
import com.xunheng.infrastructure.log.mapper.EsLogDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 日志网关实现类
 * @author: hhqkkr
 * @date: 2023/6/29 17:50
 */
@Service
@Slf4j
public class LogGatewayImpl implements LogGateway {

    @Autowired(required = false)
    private EsLogDao logDao;

    @Override
    public List<LogEntity> findByOperTimeMillisBetween(long startTime, long endTime) {
        List<EsLogDO> logs = logDao.findByOperTimeMillisBetween(startTime, endTime);
        return logs.stream().map(LogConverter::toEntity).collect(Collectors.toList());
    }

    @Override
    public LogEntity save(LogEntity entity) {
        EsLogDO esLogDO = logDao.save(LogConverter.toDB(entity));
        return LogConverter.toEntity(esLogDO);
    }

    @Override
    public void remove(String id) {
        logDao.deleteById(id);
    }

    @Override
    public void removeAll() {
        logDao.deleteAll();
    }


}
