package com.xunheng.infrastructure.system.gateway.impl;

import com.xunheng.client.system.dto.query.MatrixListQuery;
import com.xunheng.domain.matrix.gateway.MatrixGateway;
import com.xunheng.domain.matrix.model.MatrixEntity;
import com.xunheng.infrastructure.system.DO.Matrix;
import com.xunheng.infrastructure.system.convertor.MatrixConvertor;
import com.xunheng.infrastructure.system.mapper.MatrixMapper;
import com.mysql.jdbc.StringUtils;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵网关实现类
 * @author: hhqkkr
 * @date: 2023/6/30 00:34
 */
@Slf4j
@Component
public class MatrixGatewayImpl implements MatrixGateway {

    @Resource
    MatrixMapper matrixMapper;

    @Override
    public List<MatrixEntity> getAll(MatrixListQuery query) {
        List<Matrix> list = matrixMapper.allList(query);
        return list.stream().map(MatrixConvertor::toEntity).collect(Collectors.toList());
    }

    @Override
    public MatrixEntity saveOrUpdate(MatrixEntity entity) {
        Matrix matrix = MatrixConvertor.toDO(entity);
        int count = StringUtils.isNullOrEmpty(matrix.getId()) ? matrixMapper.insert(matrix) : matrixMapper.updateById(matrix);
        return MatrixConvertor.toEntity(matrix);
    }

    @Override
    public void remove(String id) {
        matrixMapper.deleteById(id);
    }
}
