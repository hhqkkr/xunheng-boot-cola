package com.xunheng.infrastructure.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xunheng.client.system.dto.query.MatrixListQuery;
import com.xunheng.infrastructure.system.DO.Matrix;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 矩阵数据处理层
 * @author hhqkkr
 * @date 2022-07-28 16:06:00
 */
@Repository
public interface MatrixMapper extends BaseMapper<Matrix> {

    /**
     * 获取所有矩阵信息列表
     * @param query 查询条件
     * @return 矩阵列表
     */
    List<Matrix> allList(@Param("qry") MatrixListQuery query);
}