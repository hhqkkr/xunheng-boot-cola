package com.xunheng.infrastructure.system.gateway.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunheng.client.system.dto.query.TenantPageQuery;
import com.xunheng.domain.core.common.utils.DateUtil;
import com.xunheng.domain.tenant.gateway.TenantGateway;
import com.xunheng.domain.tenant.model.TenantEntity;
import com.xunheng.infrastructure.system.DO.Tenant;
import com.xunheng.infrastructure.system.convertor.TenantConvertor;
import com.xunheng.infrastructure.system.mapper.TenantMapper;
import com.mysql.jdbc.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户网关实现类
 * @author: hhqkkr
 * @date: 2023/6/30 00:42
 */
@Slf4j
@Component
public class TenantGatewayImpl implements TenantGateway {

    @Autowired
    TenantMapper tenantMapper;

    @Override
    public IPage<TenantEntity> pageList(TenantPageQuery query) {
        IPage<Tenant> page = tenantMapper.pageList(new Page<Tenant>(query.getPage(), query.getPageSize()), query);
        return page.convert(TenantConvertor::toEntity);
    }

    @Override
    public TenantEntity getOneById(String id) {
        Tenant tenant = tenantMapper.selectById(id);
        return TenantConvertor.toEntity(tenant);
    }

    @Override
    public TenantEntity saveOrUpdate(TenantEntity entity) {
        Tenant tenant = TenantConvertor.toDO(entity);
        int count = StringUtils.isNullOrEmpty(tenant.getId()) ? tenantMapper.insert(tenant) : tenantMapper.updateById(tenant);
        return TenantConvertor.toEntity(tenant);
    }

    @Override
    public void remove(String id) {
        tenantMapper.deleteById(id);
    }

    @Override
    public Boolean isTenantExpire(String tenantId) {
        TenantEntity tenant = getOneById(tenantId);
        /*判断日期是否小于当前日期*/
        return DateUtil.compareDate(new Date(), tenant.getEndDate()) > 0;
    }


}
