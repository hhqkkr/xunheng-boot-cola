package com.xunheng.infrastructure.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xunheng.infrastructure.system.DO.NoticeRead;
import org.springframework.stereotype.Repository;

/**
 * 系统信息已读数据处理层
 * @author hhqkkr
 * @date 2022-04-29 12:23:58
 */
@Repository
public interface NoticeReadMapper extends BaseMapper<NoticeRead> {

}