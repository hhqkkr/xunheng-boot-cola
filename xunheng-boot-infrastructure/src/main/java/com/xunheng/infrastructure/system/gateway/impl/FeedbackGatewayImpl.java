package com.xunheng.infrastructure.system.gateway.impl;

import com.xunheng.domain.Feedback.gateway.FeedbackGateway;
import com.xunheng.domain.Feedback.model.FeedbackEntity;
import com.xunheng.infrastructure.system.DO.Feedback;
import com.xunheng.infrastructure.system.convertor.FeedbackConvertor;
import com.xunheng.infrastructure.system.mapper.FeedbackMapper;
import com.mysql.jdbc.StringUtils;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 意见反馈网关实现类
 * @author: hhqkkr
 * @date: 2023/6/30 00:36
 */
@Slf4j
@Component
public class FeedbackGatewayImpl implements FeedbackGateway {

    @Resource
    FeedbackMapper feedbackMapper;

    @Override
    public FeedbackEntity saveOrUpdate(FeedbackEntity entity) {
        Feedback feedback = FeedbackConvertor.toDO(entity);
        int count = StringUtils.isNullOrEmpty(feedback.getId()) ? feedbackMapper.insert(feedback) : feedbackMapper.updateById(feedback);
        return FeedbackConvertor.toEntity(feedback);
    }
}
