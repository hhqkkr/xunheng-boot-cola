package com.xunheng.infrastructure.system.gateway.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xunheng.domain.dict.gateway.DictGateway;
import com.xunheng.domain.dict.model.DictEntity;
import com.xunheng.infrastructure.system.DO.Dict;
import com.xunheng.infrastructure.system.convertor.DictConvertor;
import com.xunheng.infrastructure.system.mapper.DictMapper;
import com.mysql.jdbc.StringUtils;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 字典网关实现类
 * @author: hhqkkr
 * @date: 2023/6/30 00:36
 */
@Slf4j
@Component
public class DictGatewayImpl implements DictGateway {

    @Resource
    DictMapper dictMapper;

    @Override
    public List<DictEntity> getAll() {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("sort_order");
        List<Dict> dicts = dictMapper.selectList(wrapper);
        return dicts.stream().map(DictConvertor::toEntity).collect(Collectors.toList());
    }

    @Override
    public DictEntity getByType(String type) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("type",type);
        Dict dict = dictMapper.selectOne(wrapper);
        return DictConvertor.toEntity(dict);
    }

    @Override
    public DictEntity saveOrUpdate(DictEntity entity) {
        Dict dict = DictConvertor.toDO(entity);
        int count = StringUtils.isNullOrEmpty(dict.getId()) ? dictMapper.insert(dict) : dictMapper.updateById(dict);
        return DictConvertor.toEntity(dict);
    }

    @Override
    public void remove(String id) {
        dictMapper.deleteById(id);
    }

    @Override
    public Boolean checkExist(String type, String id) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        wrapper.eq("type",type);
        if(!StringUtils.isNullOrEmpty(id))wrapper.notIn("id",id);
        Dict dict = dictMapper.selectOne(wrapper);
        return dict != null;
    }
}
