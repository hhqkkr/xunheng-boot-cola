package com.xunheng.domain.user.model;

import cn.dev33.satoken.secure.BCrypt;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PassWord {

    private EncryptionPassWord encryptionPassWord;

    public PassWord(EncryptionPassWord encryptionPassWord) {
        this.encryptionPassWord = encryptionPassWord;
    }

    public PassWord(String password) {
        this.encryptionPassWord = new EncryptionPassWord(getEncryptionPassWord(password));
    }

    public static String getEncryptionPassWord(String password) {
        return new BCrypt().hashpw(password);
    }

    @Getter
    public static class EncryptionPassWord {

        private String password;

        public EncryptionPassWord(String password) {
            this.password = password;
        }
    }

    /**
     * 判断密码相等
     * @param password
     * @return
     */
    public Boolean isEqual(String password) {
        if(StringUtils.isEmpty(password))return false;
        if(!BCrypt.checkpw(password, this.encryptionPassWord.password)){
            return false;
        }
        return true;


    }
}
