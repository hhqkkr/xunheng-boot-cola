package com.xunheng.domain.tenant.model;

import com.alibaba.cola.domain.Entity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
@Entity
public class TenantAuthRecordEntity {

    @Schema(description = "id")
    private String id;

    @Schema(description = "租户id")
    private String tenantId;

    @Schema(description = "授权日期")
    private Date authDate;

    @Schema(description = "授权到期日")
    private Date endDate;

    @Schema(description = "租户名称")
    private String tenantName;
}
