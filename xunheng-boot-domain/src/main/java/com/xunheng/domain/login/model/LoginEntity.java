package com.xunheng.domain.login.model;

import cn.dev33.satoken.secure.BCrypt;
import com.alibaba.cola.domain.Entity;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.Data;

/**
 * @program: xunheng-cloud-cola
 * @description: login领域
 * @author: hhqkkr
 * @create: 2023-06-12 23:38
 */
@Data
@Entity
public class LoginEntity {

    private String id;

    private String username;

    private String password;

    private CaptchaEntity captcha;

    private String ip;

    private String saveLogin;

    private String device;

    /**
     * 校验密码
     * @param rpassword 未加密密码
     * @return
     */
    public Boolean checkPassword(String rpassword){
        if(StringUtils.isEmpty(rpassword))return false;
        if(!BCrypt.checkpw(password, rpassword)){
          return false;
        }
        return true;
    }

}
