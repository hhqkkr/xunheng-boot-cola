package com.xunheng.domain.core.satoken.config;

import cn.dev33.satoken.stp.StpInterface;
import com.xunheng.client.system.api.UserService;
import com.xunheng.client.system.dto.VO.UserRoleVO;
import com.xunheng.client.system.dto.VO.UserVO;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description:自定义权限验证接口扩展
 * @author: hhqkkr
 * @create: 2021-12-15 15:44
 */
@Component
public class CommonSaTokenInterfaceImpl implements StpInterface {

    @Resource
    private UserService userService;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        return userService.getUserPermissionCodes(loginId.toString());
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        UserVO user = userService.getUserDetailByUserId(loginId.toString());
        if(user == null)return null;
        List<String> roleNames = new ArrayList<>();
        for (UserRoleVO role : user.getRoles()) {
            roleNames.add(role.getRoleName());
        }
        return roleNames;
    }
}
