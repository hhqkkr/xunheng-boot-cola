package com.xunheng.domain.role.ability.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.client.system.dto.query.RolePageQuery;
import com.xunheng.domain.role.ability.RoleDomainService;
import com.xunheng.domain.role.gateway.RoleGateway;
import com.xunheng.domain.role.gateway.RolePermissionGateway;
import com.xunheng.domain.role.model.RoleEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class RoleDomainServiceImpl implements RoleDomainService {

    @Resource
    RoleGateway roleGateway;

    @Resource
    RolePermissionGateway rolePermissionGateway;

    @Override
    public IPage<RoleEntity> pageList(RolePageQuery query) {
        IPage<RoleEntity> page = roleGateway.pageList(query);
        /*补全权限与数据权限范围*/
        for(RoleEntity role : page.getRecords()){
            /*角色拥有菜单权限*/
            role.setPermissions(rolePermissionGateway.getByRoleId(role.getId()));
        }
        return page;
    }
}
