package com.xunheng.app.log.executor;

import com.xunheng.app.log.assembler.LogAssembler;
import com.xunheng.client.log.dto.LogCreateCmd;
import com.xunheng.client.log.dto.VO.LogVO;
import com.xunheng.domain.log.LogEntity;
import com.xunheng.domain.log.LogGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 日志创建操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 17:45
 */
@Component
public class LogCreateCmdExe {

    @Resource
    private LogGateway logGateway;

    public LogVO execute(LogCreateCmd cmd){
        LogEntity entity =  logGateway.save(LogAssembler.toEntity(cmd));
        return LogAssembler.toVo(entity);
    }
}
