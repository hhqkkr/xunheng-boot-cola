package com.xunheng.app.log;

import com.xunheng.app.log.executor.LogCreateCmdExe;
import com.xunheng.app.log.executor.LogRemoveAllCmdExe;
import com.xunheng.app.log.executor.LogRemoveCmdExe;
import com.xunheng.app.log.executor.query.LogEchartsDataQueryExe;
import com.xunheng.app.log.executor.query.LogPageQueryExe;
import com.xunheng.client.log.api.SysLogService;
import com.xunheng.client.log.dto.LogCreateCmd;
import com.xunheng.client.log.dto.VO.LogVO;
import com.xunheng.client.log.dto.query.LogPageQuery;
import jakarta.annotation.Resource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class EsLogServiceImpl implements SysLogService {

    @Resource
    private LogPageQueryExe logPageQueryExe;

    @Resource
    private LogEchartsDataQueryExe logEchartsDataQueryExe;

    @Resource
    private LogRemoveCmdExe logRemoveCmdExe;

    @Resource
    private LogRemoveAllCmdExe logRemoveAllCmdExe;

    @Resource
    private LogCreateCmdExe logCreateCmdExe;


    @Override
    public Page<LogVO> pageList(LogPageQuery query) {
        return logPageQueryExe.execute(query);
    }

    @Override
    public Map<String, Object> getEchartsData() {
        return logEchartsDataQueryExe.execute();
    }

    @Override
    public void remove(String id) {
        logRemoveCmdExe.execute(id);
    }

    @Override
    public void removeAll() {
        logRemoveAllCmdExe.execute();
    }

    @Override
    public LogVO save(LogCreateCmd cmd) {
        return logCreateCmdExe.execute(cmd);
    }
}
