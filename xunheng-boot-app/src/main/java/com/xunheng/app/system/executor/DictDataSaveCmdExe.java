
package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.DictDataAssembler;
import com.xunheng.client.system.dto.DictDataSaveCmd;
import com.xunheng.client.system.dto.VO.DictDataVO;
import com.xunheng.domain.dict.gateway.DictDataGateway;
import com.xunheng.domain.dict.model.DictDataEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 字典数据保存处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:07
 */
@Component
public class DictDataSaveCmdExe {

    @Resource
    private DictDataGateway dictDataGateway;

    public DictDataVO execute(DictDataSaveCmd cmd) {
        DictDataEntity saveEntity = DictDataAssembler.toSaveEntity(cmd);
        return DictDataAssembler.toVo(dictDataGateway.saveOrUpdate(saveEntity));
    }

}
