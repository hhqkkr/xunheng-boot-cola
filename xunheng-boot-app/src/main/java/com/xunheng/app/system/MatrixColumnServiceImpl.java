package com.xunheng.app.system;

import com.xunheng.app.system.executor.MatrixColumnSaveCmdExe;
import com.xunheng.app.system.executor.query.MatrixColumnListQueryExe;
import com.xunheng.client.system.api.MatrixColumnService;
import com.xunheng.client.system.dto.MatrixColumnSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵列信息service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class MatrixColumnServiceImpl implements MatrixColumnService {

    @Resource
    private MatrixColumnSaveCmdExe matrixColumnSaveCmdExe;
    @Resource
    MatrixColumnListQueryExe matrixColumnListQueryExe;

    @Override
    public MatrixVO saveMatrixColumn(MatrixColumnSaveCmd cmd) {
        return matrixColumnSaveCmdExe.execute(cmd);
    }

    @Override
    public MatrixVO getColumnByMatrix(String matrixId) {
        return matrixColumnListQueryExe.execute(matrixId);
    }

}