package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.client.system.dto.RoleSaveCmd;
import com.xunheng.client.system.dto.VO.RoleVO;
import com.xunheng.domain.core.satoken.util.SaUtil;
import com.xunheng.domain.role.gateway.RoleGateway;
import com.xunheng.domain.role.model.RoleEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 角色保存操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:27
 */
@Component
public class RoleSaveCmdExe {

    @Resource
    private RoleGateway roleGateway;

    @Resource
    private SaUtil saUtil;

    public RoleVO execute(RoleSaveCmd cmd) {
        RoleEntity saveEntity = RoleAssembler.toSaveEntity(cmd);
        if(cmd.getId() != null){//清除权限信息缓存
            saUtil.deleteCustomerSession("permission:*");
        }
        return RoleAssembler.toVo(roleGateway.saveOrUpdate(saveEntity));
    }
}
