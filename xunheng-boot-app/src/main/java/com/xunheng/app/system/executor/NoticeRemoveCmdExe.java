
package com.xunheng.app.system.executor;

import com.xunheng.domain.notice.gateway.NoticeGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 消息删除操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:16
 */
@Component
public class NoticeRemoveCmdExe {
    @Resource
    private NoticeGateway noticeGateway;

    public void execute(String id) {
        noticeGateway.remove(id);
    }

}
