
package com.xunheng.app.system.executor;

import com.xunheng.domain.user.ability.UserDomainService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户信息缓存刷新处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:31
 */
@Component
public class UserDetailCacheReloadCmdExe {

    @Resource
    private UserDomainService userDomainService;

    public void execute(String userId) {
        userDomainService.reloadUserDetailCache(userId);
    }

}
