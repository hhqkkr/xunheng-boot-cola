
package com.xunheng.app.system.executor;

import com.xunheng.domain.core.common.exception.GlobalException;
import com.xunheng.domain.tenant.gateway.TenantGateway;
import com.xunheng.domain.tenant.model.TenantEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户删除操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:30
 */
@Component
public class TenantRemoveCmdExe {

    @Resource
    private TenantGateway tenantGateway;

    public void execute(String id) {
        TenantEntity detail = tenantGateway.getOneById(id);
        if(detail.getEndDate() != null)throw new GlobalException("该租户已存在授权记录，无法删除.");
        tenantGateway.remove(id);
    }

}
