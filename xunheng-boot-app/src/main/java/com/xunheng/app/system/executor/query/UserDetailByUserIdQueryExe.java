package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.UserAssembler;
import com.xunheng.client.system.dto.VO.UserVO;
import com.xunheng.domain.user.ability.UserDomainService;
import com.xunheng.domain.user.model.UserEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户详情查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:02
 */
@Component
public class UserDetailByUserIdQueryExe {

    @Resource
    private UserDomainService userDomainService;

    public UserVO execute(String userId){
        UserEntity userEntity = userDomainService.getUserDetailById(userId);
        return UserAssembler.toVo(userEntity);
    }

}
