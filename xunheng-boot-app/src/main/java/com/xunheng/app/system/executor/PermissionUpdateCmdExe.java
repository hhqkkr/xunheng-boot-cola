package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.PermissionActionAssembler;
import com.xunheng.app.system.assembler.PermissionAssembler;
import com.xunheng.client.system.dto.PermissionUpdateCmd;
import com.xunheng.client.system.dto.VO.PermissionVO;
import com.xunheng.domain.core.common.exception.GlobalException;
import com.xunheng.domain.core.satoken.util.SaUtil;
import com.xunheng.domain.permission.gateway.PermissionActionGateway;
import com.xunheng.domain.permission.gateway.PermissionGateway;
import com.xunheng.domain.permission.model.PermissionEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 权限新增操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:20
 */
@Component
public class PermissionUpdateCmdExe {

    @Resource
    private PermissionGateway permissionGateway;

    @Resource
    private PermissionActionGateway permissionActionGateway;

    @Resource
    private SaUtil saUtil;

    public PermissionVO execute(PermissionUpdateCmd cmd) {
        /*校验名称不能重复*/
        PermissionEntity check = permissionGateway.getOneByName(cmd.getName());
        if(check!=null && !check.getId().equals(cmd.getId())) throw new GlobalException("名称已存在");
        PermissionEntity updateEntity = PermissionAssembler.toUpdateEntity(cmd);
        updateEntity = permissionGateway.saveOrUpdate(updateEntity);
        /*删除原来的功能列表*/
        permissionActionGateway.removeByPermissionId(updateEntity.getId());
        /*重新保存功能列表*/
        if(cmd.getActionList() != null)permissionActionGateway.batchCreate(updateEntity.getId(),cmd.getActionList().stream().map(PermissionActionAssembler::toSaveEntity).collect(Collectors.toList()));
        /*清除permission缓存*/
        saUtil.deleteCustomerSession("permission:*");
        return PermissionAssembler.toVo(updateEntity);
    }

}
