package com.xunheng.app.system.executor.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.assembler.TenantAssembler;
import com.xunheng.client.system.dto.VO.TenantVO;
import com.xunheng.client.system.dto.query.TenantPageQuery;
import com.xunheng.domain.tenant.gateway.TenantGateway;
import com.xunheng.domain.tenant.model.TenantEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户分页查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:02
 */
@Component
public class TenantPageQueryExe {

    @Resource
    TenantGateway tenantGateway;

    public IPage<TenantVO> execute(TenantPageQuery query) {
        IPage<TenantEntity> page = tenantGateway.pageList(query);
        return page.convert(TenantAssembler::toVo);
    }
}
