package com.xunheng.app.system;

import com.xunheng.app.system.executor.CustomConfigRemoveCmdExe;
import com.xunheng.app.system.executor.PersonalConfigSaveCmdExe;
import com.xunheng.app.system.executor.SystemConfigSaveCmdExe;
import com.xunheng.app.system.executor.query.SystemConfigListQueryExe;
import com.xunheng.client.system.api.CustomConfigService;
import com.xunheng.client.system.dto.PersonalConfigSaveCmd;
import com.xunheng.client.system.dto.SystemConfigSaveCmd;
import com.xunheng.client.system.dto.VO.CustomConfigVO;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 个性化配置service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class CustomConfigServiceImpl implements CustomConfigService {

    @Resource
    private PersonalConfigSaveCmdExe personalConfigSaveCmdExe;

    @Resource
    private SystemConfigSaveCmdExe systemConfigSaveCmdExe;

    @Resource
    private SystemConfigListQueryExe systemConfigListQueryExe;

    @Resource
    private CustomConfigRemoveCmdExe customConfigRemoveCmdExe;

    @Override
    public CustomConfigVO savePersonalConfig(PersonalConfigSaveCmd cmd) {
        return personalConfigSaveCmdExe.execute(cmd);
    }

    @Override
    public CustomConfigVO saveSystemConfig(SystemConfigSaveCmd cmd) {
        return systemConfigSaveCmdExe.execute(cmd);
    }

    @Override
    public List<CustomConfigVO> getSystemConfigList() {
        return systemConfigListQueryExe.execute();
    }

    @Override
    public void remove(String id) {
        customConfigRemoveCmdExe.execute(id);
    }
}
