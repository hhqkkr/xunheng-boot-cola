package com.xunheng.app.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.executor.TenantRemoveCmdExe;
import com.xunheng.app.system.executor.TenantSaveCmdExe;
import com.xunheng.app.system.executor.query.TenantDetailQueryExe;
import com.xunheng.app.system.executor.query.TenantExpireQueryExe;
import com.xunheng.app.system.executor.query.TenantPageQueryExe;
import com.xunheng.client.system.api.TenantService;
import com.xunheng.client.system.dto.TenantSaveCmd;
import com.xunheng.client.system.dto.VO.TenantVO;
import com.xunheng.client.system.dto.query.TenantPageQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class TenantServiceImpl implements TenantService {

    @Resource
    private TenantPageQueryExe tenantPageQueryExe;

    @Resource
    private TenantDetailQueryExe tenantDetailQueryExe;

    @Resource
    private TenantSaveCmdExe tenantSaveCmdExe;

    @Resource
    private TenantRemoveCmdExe tenantRemoveCmdExe;

    @Resource
    private TenantExpireQueryExe tenantExpireQueryExe;

    @Override
    public IPage<TenantVO> pageList(TenantPageQuery query){
        return tenantPageQueryExe.execute(query);
    }

    @Override
    public TenantVO getDetail(String id) {
        return tenantDetailQueryExe.execute(id);
    }

    @Override
    public TenantVO saveOrUpdate(TenantSaveCmd cmd) {
        return tenantSaveCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
        tenantRemoveCmdExe.execute(id);
    }

    @Override
    public Boolean isTenantExpire(String tenantId) {
        return tenantExpireQueryExe.execute(tenantId);
    }

}