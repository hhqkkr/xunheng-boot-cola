package com.xunheng.app.system;

import com.xunheng.app.system.executor.AuthLoginCmdExe;
import com.xunheng.app.system.executor.AuthLogoutCmdExe;
import com.xunheng.client.system.api.AuthService;
import com.xunheng.client.system.dto.AuthLoginCmd;
import com.xunheng.client.system.dto.VO.LoginVO;
import com.xunheng.domain.core.common.utils.IpUtils;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: xunheng-cloud-cola
 * @description: 网关登陆service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 12:35
 */
@Service
@Slf4j
@Transactional
public class AuthServiceImpl implements AuthService {

    @Resource
    private AuthLoginCmdExe authLoginCmdExe;

    @Resource
    private AuthLogoutCmdExe authLogoutCmdExe;

    @Override
    public LoginVO login(AuthLoginCmd cmd, HttpServletRequest request) {
        //webflux方式获取ip
        cmd.setIp(IpUtils.getIpAddr(request));
        return authLoginCmdExe.execute(cmd);
    }

    @Override
    public void logout() {
        authLogoutCmdExe.execute();
    }
}
