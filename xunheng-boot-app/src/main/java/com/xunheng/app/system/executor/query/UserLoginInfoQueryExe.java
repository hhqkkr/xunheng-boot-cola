package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.UserAssembler;
import com.xunheng.client.system.dto.VO.UserLoginVO;
import com.xunheng.domain.user.gateway.UserGateway;
import com.xunheng.domain.user.model.UserEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户登录校验信息查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:02
 */
@Component
public class UserLoginInfoQueryExe {

    @Resource
    private UserGateway userGateway;

    public UserLoginVO execute(String username){
        UserEntity entity = userGateway.getOneByUsername(username);
        return UserAssembler.toUserLoginVO(entity);
    }

}
