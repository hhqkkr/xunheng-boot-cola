package com.xunheng.app.system;

import com.xunheng.app.system.executor.CaptchaLoadCmdExe;
import com.xunheng.client.system.api.CaptchaService;
import com.xunheng.client.system.dto.VO.CaptchaVO;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: xunheng-cloud-cola
 * @description: 登录验证码service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 12:38
 */
@Service
@Slf4j
@Transactional
public class CaptchaServiceImpl implements CaptchaService {

    @Resource
    private CaptchaLoadCmdExe captchaLoadCmdExe;

    @Override
    public CaptchaVO loadCaptcha() {
        return captchaLoadCmdExe.execute();
    }

}
