package com.xunheng.app.system.executor;

import cn.dev33.satoken.stp.StpUtil;
import com.xunheng.client.system.dto.VO.UserVO;
import com.xunheng.app.system.assembler.UserAssembler;
import com.xunheng.domain.user.ability.UserDomainService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 根据token获取用户信息
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Component
public class UserInfoByTokenQueryExe {

    @Resource
    private UserDomainService userDomainService;

    public UserVO execute(String token){
        String userId = String.valueOf(StpUtil.getLoginIdByToken(token));
        return UserAssembler.toVo(userDomainService.getUserDetailById(userId));
    }

}
