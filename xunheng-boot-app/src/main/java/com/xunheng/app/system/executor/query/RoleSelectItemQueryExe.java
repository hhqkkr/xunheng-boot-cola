package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.client.system.dto.VO.SelectItemVO;
import com.xunheng.domain.role.gateway.RoleGateway;
import com.xunheng.domain.role.model.RoleEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 角色选择器查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:56
 */
@Component
public class RoleSelectItemQueryExe {

    @Resource
    private RoleGateway roleGateway;


    public List<SelectItemVO> execute(){
        List<RoleEntity> list = roleGateway.getSelectItem();
        return list.stream().map(RoleAssembler::toSelectItem).collect(Collectors.toList());
    }


}
