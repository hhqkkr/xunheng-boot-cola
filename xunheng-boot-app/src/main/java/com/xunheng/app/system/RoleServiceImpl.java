package com.xunheng.app.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.executor.RoleEditPermCmdExe;
import com.xunheng.app.system.executor.RoleRemoveCmdExe;
import com.xunheng.app.system.executor.RoleSaveCmdExe;
import com.xunheng.app.system.executor.query.*;
import com.xunheng.client.system.api.RoleService;
import com.xunheng.client.system.dto.RolePermEditCmd;
import com.xunheng.client.system.dto.RoleSaveCmd;
import com.xunheng.client.system.dto.VO.RoleVO;
import com.xunheng.client.system.dto.VO.SelectItemVO;
import com.xunheng.client.system.dto.query.RolePageQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 角色service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleSelectItemQueryExe roleSelectItemQueryExe;

    @Resource
    RolePageQueryExe rolePageQueryExe;

    @Resource
    RoleListQueryExe roleListQueryExe;

    @Resource
    RoleEditPermCmdExe roleEditPermCmdExe;

    @Resource
    RoleSaveCmdExe roleSaveCmdExe;

    @Resource
    RoleRemoveCmdExe roleRemoveCmdExe;

    @Resource
    RoleByUserAndActionQueryExe roleByUserAndActionQueryExe;

    @Override
    public List<SelectItemVO> getSelectItem() {
        return roleSelectItemQueryExe.execute();
    }

    @Override
    public IPage<RoleVO> pageList(RolePageQuery query) {
        return rolePageQueryExe.execute(query);
    }

    @Override
    public List<RoleVO> getAll() {
        return roleListQueryExe.execute();
    }

    @Override
    public void editRolePerm(RolePermEditCmd cmd) {
        roleEditPermCmdExe.execute(cmd);
    }

    @Override
    public RoleVO saveOrUpdate(RoleSaveCmd cmd) {
        return roleSaveCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
        roleRemoveCmdExe.execute(id);
    }

    @Override
    public List<RoleVO> getRoleByUserAndAction(String userId, String code) {
        return roleByUserAndActionQueryExe.execute(userId,code);
    }
}