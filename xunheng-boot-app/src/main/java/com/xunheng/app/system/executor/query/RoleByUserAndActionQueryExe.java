package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.client.system.dto.VO.RoleVO;
import com.xunheng.domain.role.gateway.RoleGateway;
import com.xunheng.domain.role.model.RoleEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 根据用户与操作码查询角色处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:51
 */
@Component
public class RoleByUserAndActionQueryExe {

    @Resource
    private RoleGateway roleGateway;

    public List<RoleVO> execute(String userId, String code){
        /*查询出角色列表*/
        List<RoleEntity> roleList = roleGateway.getRoleByUserAndAction(userId, code);
        return roleList.stream().map(RoleAssembler::toVo).collect(Collectors.toList());
    }
}
