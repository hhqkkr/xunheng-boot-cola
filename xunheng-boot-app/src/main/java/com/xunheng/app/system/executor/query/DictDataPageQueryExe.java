package com.xunheng.app.system.executor.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.assembler.DictDataAssembler;
import com.xunheng.client.system.dto.VO.DictDataVO;
import com.xunheng.client.system.dto.query.DictDataPageQuery;
import com.xunheng.domain.dict.gateway.DictDataGateway;
import com.xunheng.domain.dict.model.DictDataEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;


/**
 * @program: xunheng-cloud-cola
 * @description: 字典数据分页查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:40
 */
@Component
public class DictDataPageQueryExe {
    @Resource
    DictDataGateway dictDataGateway;

    public IPage<DictDataVO> execute(DictDataPageQuery query) {
        IPage<DictDataEntity> page = dictDataGateway.pageList(query);
        return page.convert(DictDataAssembler::toVo);
    }
}
