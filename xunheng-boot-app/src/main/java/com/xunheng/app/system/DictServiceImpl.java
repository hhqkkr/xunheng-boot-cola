package com.xunheng.app.system;

import com.xunheng.app.system.executor.DictRemoveCmdExe;
import com.xunheng.app.system.executor.DictSaveCmdExe;
import com.xunheng.app.system.executor.query.DictListQueryExe;
import com.xunheng.client.system.api.DictService;
import com.xunheng.client.system.dto.DictSaveCmd;
import com.xunheng.client.system.dto.VO.DictVO;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 字典信息service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class DictServiceImpl implements DictService {

    @Resource
    private DictListQueryExe dictListQueryExe;

    @Resource
    DictSaveCmdExe dictSaveCmdExe;

    @Resource
    DictRemoveCmdExe dictRemoveCmdExe;

    @Override
    public List<DictVO> getAll() {
        return dictListQueryExe.execute();
    }

    @Override
    public DictVO saveOrUpdate(DictSaveCmd cmd) {
        return dictSaveCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
        dictRemoveCmdExe.execute(id);
    }
}