package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.CustomConfigAssembler;
import com.xunheng.client.system.dto.VO.CustomConfigVO;
import com.xunheng.domain.customConfig.gateway.CustomConfigGateway;
import com.xunheng.domain.customConfig.model.CustomConfigEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 系统拓展配置查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:58
 */
@Component
public class SystemConfigListQueryExe {
    @Resource
    CustomConfigGateway customConfigGateway;

    public List<CustomConfigVO> execute() {
        List<CustomConfigEntity> list = customConfigGateway.getSystemConfigList();
        return list.stream().map(CustomConfigAssembler::toVo).collect(Collectors.toList());
    }
}
