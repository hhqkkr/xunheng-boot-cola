package com.xunheng.app.system.executor.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.assembler.TenantAuthRecordAssembler;
import com.xunheng.client.system.dto.VO.TenantAuthRecordVO;
import com.xunheng.client.system.dto.query.TenantAuthRecordPageQuery;
import com.xunheng.domain.tenant.gateway.TenantAuthRecordGateway;
import com.xunheng.domain.tenant.model.TenantAuthRecordEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户授权记录分页查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:00
 */
@Component
public class TenantAuthRecordPageQueryExe {
    
    @Resource
    TenantAuthRecordGateway tenantAuthRecordGateway;

    public IPage<TenantAuthRecordVO> execute(TenantAuthRecordPageQuery query) {
        IPage<TenantAuthRecordEntity> page = tenantAuthRecordGateway.pageList(query);
        return page.convert(TenantAuthRecordAssembler::toVo);
    }
}
