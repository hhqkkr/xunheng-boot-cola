
package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.OrganizationAssembler;
import com.xunheng.client.system.dto.OrganizationUpdateCmd;
import com.xunheng.client.system.dto.VO.OrganizationVO;
import com.xunheng.domain.organization.ability.OrganizationDomainService;
import com.xunheng.domain.organization.model.OrganizationEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 组织架构更新操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:17
 */
@Component
public class OrganizationUpdateCmdExe {

    @Resource
    OrganizationDomainService organizationDomainService;

    public OrganizationVO execute(OrganizationUpdateCmd cmd) {
        OrganizationEntity updateEntity = OrganizationAssembler.toUpdateEntity(cmd);
        return OrganizationAssembler.toVo(organizationDomainService.update(updateEntity));
    }

}
