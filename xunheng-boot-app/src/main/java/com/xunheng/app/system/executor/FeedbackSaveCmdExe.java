
package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.FeedbackAssembler;
import com.xunheng.client.system.dto.FeedbackSaveCmd;
import com.xunheng.client.system.dto.VO.FeedbackVO;
import com.xunheng.domain.Feedback.gateway.FeedbackGateway;
import com.xunheng.domain.Feedback.model.FeedbackEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 意见反馈保存处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:08
 */
@Component
public class FeedbackSaveCmdExe {

    @Resource
    private FeedbackGateway feedbackGateway;

    public FeedbackVO execute(FeedbackSaveCmd cmd) {
        FeedbackEntity saveEntity = FeedbackAssembler.toSaveEntity(cmd);
        return FeedbackAssembler.toVo(feedbackGateway.saveOrUpdate(saveEntity));
    }

}
