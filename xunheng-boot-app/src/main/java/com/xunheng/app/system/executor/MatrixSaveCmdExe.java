package com.xunheng.app.system.executor;


import com.xunheng.app.system.assembler.MatrixAssembler;
import com.xunheng.client.system.dto.MatrixSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.domain.matrix.gateway.MatrixGateway;
import com.xunheng.domain.matrix.model.MatrixEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵信息保存操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:13
 */
@Component
public class MatrixSaveCmdExe {

    @Resource
    private MatrixGateway matrixGateway;

    public MatrixVO execute(MatrixSaveCmd cmd) {
        MatrixEntity saveEntity = MatrixAssembler.toSaveEntity(cmd);
        return MatrixAssembler.toVo( matrixGateway.saveOrUpdate(saveEntity));
    }
}
