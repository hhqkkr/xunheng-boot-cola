package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.PermissionAssembler;
import com.xunheng.client.system.dto.VO.PermissionVO;
import com.xunheng.domain.permission.ability.PermissionDomainService;
import com.xunheng.domain.permission.model.PermissionEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 系统权限查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:58
 */
@Component
public class SystemPermissionListQueryExe {

    @Resource
    private PermissionDomainService permissionDomainService;

    public List<PermissionVO> execute(){
        List<PermissionEntity> systemPermissionList = permissionDomainService.getSystemPermissionList();
        return PermissionAssembler.toTree(systemPermissionList.stream().map(PermissionAssembler::toVo).collect(Collectors.toList()));
    }

}
