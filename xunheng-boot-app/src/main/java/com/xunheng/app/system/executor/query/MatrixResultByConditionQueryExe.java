package com.xunheng.app.system.executor.query;

import com.xunheng.client.system.dto.query.MatrixResultByConditionQuery;
import com.xunheng.domain.matrix.gateway.MatrixDataGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵匹配结果查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:49
 */
@Component
public class MatrixResultByConditionQueryExe {

    @Resource
    private MatrixDataGateway matrixDataGateway;

    public List<String> execute(MatrixResultByConditionQuery query) {
        return matrixDataGateway.searchByCondition(query);
    }
}
