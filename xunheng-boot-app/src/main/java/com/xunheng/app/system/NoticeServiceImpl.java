package com.xunheng.app.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.executor.NoticeMarkReadAllCmdExe;
import com.xunheng.app.system.executor.NoticeMarkReadCmdExe;
import com.xunheng.app.system.executor.NoticeRemoveCmdExe;
import com.xunheng.app.system.executor.query.NoticePageQueryExe;
import com.xunheng.app.system.executor.query.NoticeUnreadListQueryExe;
import com.xunheng.client.system.api.NoticeService;
import com.xunheng.client.system.dto.VO.NoticeVO;
import com.xunheng.client.system.dto.query.NoticePageQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 消息通知service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticePageQueryExe noticePageQueryExe;

    @Resource
    private NoticeUnreadListQueryExe noticeUnreadListQueryExe;

    @Resource
    private NoticeRemoveCmdExe noticeRemoveCmdExe;

    @Resource
    private NoticeMarkReadCmdExe noticeMarkReadCmdExe;

    @Resource
    private NoticeMarkReadAllCmdExe noticeMarkReadAllCmdExe;

    @Override
    public IPage<NoticeVO> pageList(NoticePageQuery query) {
        return noticePageQueryExe.execute(query);
    }

    @Override
    public List<NoticeVO> getUnReadNoticeList() {
        return noticeUnreadListQueryExe.execute();
    }

    @Override
    public void markRead(String id) {
         noticeMarkReadCmdExe.execute(id);
    }

    @Override
    public void markReadAll() {
        noticeMarkReadAllCmdExe.execute();
    }

    @Override
    public void remove(String id) {
        noticeRemoveCmdExe.execute(id);
    }
}