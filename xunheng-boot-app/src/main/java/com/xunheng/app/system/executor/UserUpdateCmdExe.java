
package com.xunheng.app.system.executor;

import com.xunheng.client.system.dto.VO.UserVO;
import com.xunheng.app.system.assembler.UserAssembler;
import com.xunheng.client.system.dto.UserUpdateCmd;
import com.xunheng.domain.user.ability.UserDomainService;
import com.xunheng.domain.user.gateway.UserGateway;
import com.xunheng.domain.user.model.UserEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户更新操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:31
 */
@Component
public class UserUpdateCmdExe {

    @Resource
    private UserGateway userGateway;

    @Resource
    private UserDomainService userDomainService;

    public UserVO execute(UserUpdateCmd cmd) {
        UserEntity updateEntity = UserAssembler.toUpdateEntity(cmd);
        updateEntity = userGateway.saveOrUpdate(updateEntity);
        userDomainService.reloadUserDetailCache(cmd.getId());//刷新用户信息缓存
        return UserAssembler.toVo(updateEntity);
    }

}
