package com.xunheng.app.system;

import com.xunheng.app.system.executor.PermissionCreateCmdExe;
import com.xunheng.app.system.executor.PermissionRemoveCmdExe;
import com.xunheng.app.system.executor.PermissionUpdateCmdExe;
import com.xunheng.app.system.executor.query.SystemPermissionListQueryExe;
import com.xunheng.app.system.executor.query.UserPermissionQueryExe;
import com.xunheng.client.system.api.PermissionService;
import com.xunheng.client.system.dto.PermissionCreateCmd;
import com.xunheng.client.system.dto.PermissionUpdateCmd;
import com.xunheng.client.system.dto.VO.PermissionVO;
import com.xunheng.client.system.dto.VO.UserPermissionVO;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 权限service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Resource
    private PermissionCreateCmdExe permissionCreateCmdExe;

    @Resource
    private PermissionUpdateCmdExe permissionUpdateCmdExe;

    @Resource
    private PermissionRemoveCmdExe permissionRemoveCmdExe;

    @Resource
    private UserPermissionQueryExe userPermissionQueryExe;

    @Resource
    private SystemPermissionListQueryExe systemPermissionListQueryExe;

    @Override
    public UserPermissionVO getUserPermission() {
        return userPermissionQueryExe.execute();
    }

    @Override
    public List<PermissionVO> getSystemPermissionList() {
        return systemPermissionListQueryExe.execute();
    }

    @Override
    public PermissionVO create(PermissionCreateCmd cmd) {
        return permissionCreateCmdExe.execute(cmd);
    }

    @Override
    public PermissionVO update(PermissionUpdateCmd cmd) {
        return permissionUpdateCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
        permissionRemoveCmdExe.execute(id);
    }
}