package com.xunheng.app.system.executor.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.client.system.dto.VO.UserVO;
import com.xunheng.app.system.assembler.UserAssembler;
import com.xunheng.client.system.dto.query.UserPageQuery;
import com.xunheng.domain.user.gateway.UserGateway;
import com.xunheng.domain.user.gateway.UserRoleGateway;
import com.xunheng.domain.user.model.UserEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户信息分页查询处理器去
 * @author: hhqkkr
 * @date: 2023/6/29 23:03
 */
@Component
public class UserPageQueryExe {

    @Resource
    private UserGateway userGateway;

    @Resource
    private UserRoleGateway userRoleGateway;

    public IPage<UserVO> execute(UserPageQuery query) {
        IPage<UserEntity> page = userGateway.pageList(query);
        page.convert(u ->{//绑定角色信息
            u.bindRoleInfo(userRoleGateway.getByUserId(u.getId()));
            return u;
        });
        return page.convert(UserAssembler::toVo);
    }
}
