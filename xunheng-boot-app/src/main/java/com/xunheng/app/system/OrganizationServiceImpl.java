package com.xunheng.app.system;

import com.xunheng.app.system.executor.OrganizationCreateCmdExe;
import com.xunheng.app.system.executor.OrganizationRemoveCmdExe;
import com.xunheng.app.system.executor.OrganizationUpdateCmdExe;
import com.xunheng.app.system.executor.query.OrganizationListQueryExe;
import com.xunheng.client.system.api.OrganizationService;
import com.xunheng.client.system.dto.OrganizationCreateCmd;
import com.xunheng.client.system.dto.OrganizationUpdateCmd;
import com.xunheng.client.system.dto.VO.OrganizationVO;
import com.xunheng.client.system.dto.query.OrganizationListQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 组织架构service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class OrganizationServiceImpl implements OrganizationService {

    @Resource
    private OrganizationListQueryExe organizationListQueryExe;

    @Resource
    private OrganizationCreateCmdExe organizationCreateCmdExe;

    @Resource
    private OrganizationUpdateCmdExe organizationUpdateCmdExe;

    @Resource
    private OrganizationRemoveCmdExe organizationRemoveCmdExe;

    @Override
    public List<OrganizationVO> getGroupOrganizationList(OrganizationListQuery query) {
        return organizationListQueryExe.execute(query);
    }

    @Override
    public OrganizationVO create(OrganizationCreateCmd cmd) {
        return organizationCreateCmdExe.execute(cmd);
    }

    @Override
    public OrganizationVO update(OrganizationUpdateCmd cmd) {
        return organizationUpdateCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
         organizationRemoveCmdExe.execute(id);
    }
}