package com.xunheng.app.system.executor;

import cn.dev33.satoken.stp.StpUtil;
import com.xunheng.client.system.api.UserService;
import com.xunheng.client.system.dto.VO.UserVO;
import com.xunheng.client.log.dto.LogCreateCmd;
import com.xunheng.domain.log.LogEntity;
import com.xunheng.domain.log.LogGateway;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 异步保存日志操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 17:11
 */
@Slf4j
@Component
public class AsyncLogSaveCmdExe
{


    @Resource
    UserService userService;

    @Resource
    LogGateway logGateway;

    @Async
    public void execute(LogCreateCmd cmd){
        log.info(cmd.getTitle());
        /*补全用户信息*/
        if(StpUtil.isLogin()){
            UserVO user = userService.getUserDetailByUserId(StpUtil.getLoginId(""));
            if (user != null){
                cmd.setOperName(user.getUsername());
                cmd.setDepartmentTitle(user.getDepartmentTitle());
                cmd.setTenantTitle(user.getTenantTitle());
            }
        }
        /*保存*/
        logGateway.save(LogEntity.createEntity(cmd));
    }

}
