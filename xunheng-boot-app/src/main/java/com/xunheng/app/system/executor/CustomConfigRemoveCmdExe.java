
package com.xunheng.app.system.executor;

import com.xunheng.domain.customConfig.gateway.CustomConfigGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;


/**
 * @program: xunheng-cloud-cola
 * @description: 个性化配置删除处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:06
 */
@Component
public class CustomConfigRemoveCmdExe {

    @Resource
    private CustomConfigGateway customConfigGateway;

    public void execute(String id) {
        customConfigGateway.remove(id);
    }

}
