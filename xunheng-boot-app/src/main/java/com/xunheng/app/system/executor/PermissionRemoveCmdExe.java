
package com.xunheng.app.system.executor;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.xunheng.domain.core.common.exception.GlobalException;
import com.xunheng.domain.core.satoken.util.SaUtil;
import com.xunheng.domain.permission.gateway.PermissionGateway;
import com.xunheng.domain.role.gateway.RolePermissionGateway;
import com.xunheng.domain.role.model.RolePermissionEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 权限删除操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:19
 */
@Component
public class PermissionRemoveCmdExe {

    @Resource
    private PermissionGateway permissionGateway;

    @Resource
    private RolePermissionGateway rolePermissionGateway;

    @Resource
    private SaUtil saUtil;

    public void execute(String id) {
        /*有角色在使用的 不能删除*/
        List<RolePermissionEntity> list = rolePermissionGateway.getByPermissionId(id);
        if(CollectionUtils.isNotEmpty(list)){
            throw new GlobalException("删除失败，包含正被角色使用关联的菜单或权限");
        }
        /*删除*/
        permissionGateway.remove(id);
        /*清除权限缓存*/
        saUtil.deleteCustomerSession("permission:*");
    }

}
