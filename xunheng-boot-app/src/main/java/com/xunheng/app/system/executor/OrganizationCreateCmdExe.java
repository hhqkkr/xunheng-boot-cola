
package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.OrganizationAssembler;
import com.xunheng.client.system.dto.OrganizationCreateCmd;
import com.xunheng.client.system.dto.VO.OrganizationVO;
import com.xunheng.domain.organization.ability.OrganizationDomainService;
import com.xunheng.domain.organization.model.OrganizationEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 组织架构新增操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:16
 */
@Component
public class OrganizationCreateCmdExe {

    @Resource
    OrganizationDomainService organizationDomainService;

    public OrganizationVO execute(OrganizationCreateCmd cmd) {
        OrganizationEntity createEntity = OrganizationAssembler.toCreateEntity(cmd);
        return OrganizationAssembler.toVo(organizationDomainService.create(createEntity));
    }

}
