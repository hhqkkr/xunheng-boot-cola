package com.xunheng.app.system;

import com.xunheng.app.system.executor.MatrixRemoveCmdExe;
import com.xunheng.app.system.executor.MatrixSaveCmdExe;
import com.xunheng.app.system.executor.query.MatrixListQueryExe;
import com.xunheng.client.system.api.MatrixService;
import com.xunheng.client.system.dto.MatrixSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.client.system.dto.query.MatrixListQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵信息service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class MatrixServiceImpl implements MatrixService {

    @Resource
    private MatrixSaveCmdExe matrixSaveCmdExe;

    @Resource
    private MatrixRemoveCmdExe matrixRemoveCmdExe;

    @Resource
    private MatrixListQueryExe matrixListQueryExe;

    @Override
    public List<MatrixVO> getAll(MatrixListQuery query) {
        return matrixListQueryExe.execute(query);
    }

    @Override
    public MatrixVO saveOrUpdate(MatrixSaveCmd cmd) {
        return matrixSaveCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
        matrixRemoveCmdExe.execute(id);
    }
}