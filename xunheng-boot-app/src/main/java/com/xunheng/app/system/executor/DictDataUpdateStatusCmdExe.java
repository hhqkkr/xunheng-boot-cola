
package com.xunheng.app.system.executor;

import com.xunheng.domain.dict.gateway.DictDataGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 字典数据状态更新处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:07
 */
@Component
public class DictDataUpdateStatusCmdExe {
    @Resource
    private DictDataGateway dictDataGateway;

    public void execute(String id, Integer status) {
        dictDataGateway.updateStatus(id,status);
    }

}
