package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.TenantAuthRecordAssembler;
import com.xunheng.client.system.dto.VO.TenantAuthRecordVO;
import com.xunheng.domain.tenant.gateway.TenantAuthRecordGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户授权记录详情查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:59
 */
@Component
public class TenantAuthRecordDetailQueryExe {

    @Resource
    TenantAuthRecordGateway tenantAuthRecordGateway;

    public TenantAuthRecordVO execute(String id) {
        return TenantAuthRecordAssembler.toVo(tenantAuthRecordGateway.getOneById(id));
    }
}
