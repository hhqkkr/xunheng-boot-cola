package com.xunheng.app.system.executor.query;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.client.system.dto.VO.RoleVO;
import com.xunheng.client.system.dto.query.RolePageQuery;
import com.xunheng.domain.role.ability.RoleDomainService;
import com.xunheng.domain.role.model.RoleEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 角色分页查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:55
 */
@Component
public class RolePageQueryExe {

    @Resource
    private RoleDomainService roleDomainService;

    public IPage<RoleVO> execute(RolePageQuery query) {
        IPage<RoleEntity> page = roleDomainService.pageList(query);
        return page.convert(RoleAssembler::toVo);
    }
}
