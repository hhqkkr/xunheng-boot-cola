
package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.CustomConfigAssembler;
import com.xunheng.client.system.dto.SystemConfigSaveCmd;
import com.xunheng.client.system.dto.VO.CustomConfigVO;
import com.xunheng.domain.customConfig.gateway.CustomConfigGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 系统拓展配置保存处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:28
 */
@Component
public class SystemConfigSaveCmdExe {
    @Resource
    private CustomConfigGateway customConfigGateway;

    public CustomConfigVO execute(SystemConfigSaveCmd cmd) {
        return CustomConfigAssembler.toVo(customConfigGateway.saveOrUpdate(CustomConfigAssembler.toSystemSaveEntity(cmd)));
    }

}
