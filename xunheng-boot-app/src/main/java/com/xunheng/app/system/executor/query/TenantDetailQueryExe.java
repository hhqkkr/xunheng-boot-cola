package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.TenantAssembler;
import com.xunheng.client.system.dto.VO.TenantVO;
import com.xunheng.domain.tenant.gateway.TenantGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户详情查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:00
 */
@Component
public class TenantDetailQueryExe {
    @Resource
    TenantGateway tenantGateway;

    public TenantVO execute(String id) {
        return TenantAssembler.toVo(tenantGateway.getOneById(id));
    }
}
