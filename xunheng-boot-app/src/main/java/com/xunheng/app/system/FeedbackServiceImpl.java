package com.xunheng.app.system;

import com.xunheng.app.system.executor.FeedbackSaveCmdExe;
import com.xunheng.client.system.api.FeedbackService;
import com.xunheng.client.system.dto.FeedbackSaveCmd;
import com.xunheng.client.system.dto.VO.FeedbackVO;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @program: xunheng-cloud-cola
 * @description: 意见反馈service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class FeedbackServiceImpl implements FeedbackService {

    @Resource
    private FeedbackSaveCmdExe feedbackSaveCmdExe;

    @Override
    public FeedbackVO saveOrUpdate(FeedbackSaveCmd cmd) {
        return feedbackSaveCmdExe.execute(cmd);
    }
}
