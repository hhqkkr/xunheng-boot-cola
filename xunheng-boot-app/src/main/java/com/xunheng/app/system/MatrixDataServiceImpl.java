package com.xunheng.app.system;

import com.xunheng.app.system.executor.MatrixDataSaveCmdExe;
import com.xunheng.app.system.executor.query.MatrixDataListQueryExe;
import com.xunheng.app.system.executor.query.MatrixResultByConditionQueryExe;
import com.xunheng.client.system.api.MatrixDataService;
import com.xunheng.client.system.dto.MatrixDataSaveCmd;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.client.system.dto.query.MatrixResultByConditionQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵数据service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class MatrixDataServiceImpl implements MatrixDataService {

    @Resource
    private MatrixDataSaveCmdExe matrixDataSaveCmdExe;

    @Resource
    MatrixDataListQueryExe matrixDataListQueryExe;

    @Resource
    MatrixResultByConditionQueryExe matrixResultByConditionQueryExe;


    @Override
    public MatrixVO saveMatrixData(MatrixDataSaveCmd cmd) {
        return matrixDataSaveCmdExe.execute(cmd);
    }

    @Override
    public MatrixVO getConfigDataByMatrix(String matrixId) {
        return matrixDataListQueryExe.execute(matrixId);
    }

    @Override
    public List<String> getResultByCondition(MatrixResultByConditionQuery query) {
        return matrixResultByConditionQueryExe.execute(query);
    }

}