package com.xunheng.app.system.executor.query;

import cn.dev33.satoken.stp.StpUtil;
import com.xunheng.app.system.assembler.NoticeAssembler;
import com.xunheng.client.system.dto.VO.NoticeVO;
import com.xunheng.domain.notice.gateway.NoticeGateway;
import com.xunheng.domain.notice.model.NoticeEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 消息未读列表查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:50
 */
@Component
public class NoticeUnreadListQueryExe {

    @Resource
    NoticeGateway noticeGateway;

    public List<NoticeVO> execute() {
        List<NoticeEntity> list = noticeGateway.getUnReadNoticeList(StpUtil.getLoginId("-1"));
        return list.stream().map(NoticeAssembler::toVo).collect(Collectors.toList());
    }
}
