package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.MatrixAssembler;
import com.xunheng.client.system.dto.VO.MatrixVO;
import com.xunheng.client.system.dto.query.MatrixListQuery;
import com.xunheng.domain.matrix.gateway.MatrixGateway;
import com.xunheng.domain.matrix.model.MatrixEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 矩阵信息查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:48
 */
@Component
public class MatrixListQueryExe {
    @Resource
    MatrixGateway matrixGateway;

    public List<MatrixVO> execute(MatrixListQuery query) {
        List<MatrixEntity> list = matrixGateway.getAll(query);
        return list.stream().map(MatrixAssembler::toVo).collect(Collectors.toList());
    }
}
