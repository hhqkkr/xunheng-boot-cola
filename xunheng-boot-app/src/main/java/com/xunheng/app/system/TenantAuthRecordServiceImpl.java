package com.xunheng.app.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xunheng.app.system.executor.TenantAuthRecordRemoveCmdExe;
import com.xunheng.app.system.executor.TenantAuthRecordSaveCmdExe;
import com.xunheng.app.system.executor.query.TenantAuthRecordDetailQueryExe;
import com.xunheng.app.system.executor.query.TenantAuthRecordPageQueryExe;
import com.xunheng.client.system.api.TenantAuthRecordService;
import com.xunheng.client.system.dto.TenantAuthRecordSaveCmd;
import com.xunheng.client.system.dto.VO.TenantAuthRecordVO;
import com.xunheng.client.system.dto.query.TenantAuthRecordPageQuery;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户授权记录service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class TenantAuthRecordServiceImpl implements TenantAuthRecordService {

    @Resource
    private TenantAuthRecordPageQueryExe tenantAuthRecordPageQueryExe;

    @Resource
    private TenantAuthRecordDetailQueryExe tenantAuthRecordDetailQueryExe;

    @Resource
    TenantAuthRecordSaveCmdExe tenantAuthRecordSaveCmdExe;

    @Resource
    TenantAuthRecordRemoveCmdExe tenantAuthRecordRemoveCmdExe;

    @Override
    public IPage<TenantAuthRecordVO> pageList(TenantAuthRecordPageQuery query) {
        return tenantAuthRecordPageQueryExe.execute(query);
    }

    @Override
    public TenantAuthRecordVO getDetail(String id) {
        return tenantAuthRecordDetailQueryExe.execute(id);
    }

    @Override
    public TenantAuthRecordVO saveOrUpdate(TenantAuthRecordSaveCmd cmd) {
        return tenantAuthRecordSaveCmdExe.execute(cmd);
    }

    @Override
    public void remove(String id) {
        tenantAuthRecordRemoveCmdExe.execute(id);
    }
}