package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.PermissionAssembler;
import com.xunheng.client.system.dto.PermissionCreateCmd;
import com.xunheng.client.system.dto.VO.PermissionVO;
import com.xunheng.domain.core.common.exception.GlobalException;
import com.xunheng.domain.permission.gateway.PermissionGateway;
import com.xunheng.domain.permission.model.PermissionEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 权限新增操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:18
 */
@Component
public class PermissionCreateCmdExe {

    @Resource
    private PermissionGateway permissionGateway;

    public PermissionVO execute(PermissionCreateCmd cmd) {
        /*校验名称是否存在*/
        if(permissionGateway.getOneByName(cmd.getName()) != null) throw new GlobalException("名称已存在");
        PermissionEntity createEntity = PermissionAssembler.toCreateEntity(cmd);
        return PermissionAssembler.toVo(permissionGateway.saveOrUpdate(createEntity));
    }

}
