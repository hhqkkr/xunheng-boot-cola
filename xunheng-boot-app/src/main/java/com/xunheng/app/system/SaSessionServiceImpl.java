package com.xunheng.app.system;

import com.xunheng.app.system.executor.SaSessionChangeCmdExe;
import com.xunheng.app.system.executor.SaSessionKickOutCmdExe;
import com.xunheng.app.system.executor.query.SaSessionListQueryExe;
import com.xunheng.client.system.api.SaSessionService;
import com.xunheng.client.system.dto.VO.SaSessionVO;
import org.springframework.transaction.annotation.Transactional;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: xunheng-cloud-cola
 * @description: sasession service实现类
 * @author: hhqkkr
 * @date: 2023/6/29 23:37
 */
@Service
@Transactional
public class SaSessionServiceImpl implements SaSessionService {

    @Resource
    private SaSessionListQueryExe saSessionListQueryExe;

    @Resource
    private SaSessionKickOutCmdExe saSessionKickOutCmdExe;

    @Resource
    private SaSessionChangeCmdExe saSessionChangeCmdExe;

    @Override
    public List<SaSessionVO> getAll(String username) {
        return saSessionListQueryExe.execute(username);
    }

    @Override
    public void kickOutByToken(String token) {
        saSessionKickOutCmdExe.execute(token);
    }

    @Override
    public void changeSession(String token) {
        saSessionChangeCmdExe.execute(token);
    }
}