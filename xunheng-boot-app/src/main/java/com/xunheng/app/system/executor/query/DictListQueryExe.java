package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.DictAssembler;
import com.xunheng.client.system.dto.VO.DictVO;
import com.xunheng.domain.dict.gateway.DictGateway;
import com.xunheng.domain.dict.model.DictEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @program: xunheng-cloud-cola
 * @description: 字典信息查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:42
 */
@Component
public class DictListQueryExe {

    @Resource
    DictGateway dictGateway;

    public List<DictVO> execute() {
        List<DictEntity> list = dictGateway.getAll();
        return list.stream().map(DictAssembler::toVo).collect(Collectors.toList());
    }
}
