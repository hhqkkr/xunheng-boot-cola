
package com.xunheng.app.system.executor;

import cn.dev33.satoken.stp.StpUtil;
import com.xunheng.client.system.dto.UserPasswordUpdateCmd;
import com.xunheng.domain.core.common.exception.GlobalException;
import com.xunheng.domain.user.ability.UserDomainService;
import com.xunheng.domain.user.gateway.UserGateway;
import com.xunheng.domain.user.model.PassWord;
import com.xunheng.domain.user.model.UserEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户更新密码操作处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:36
 */
@Component
public class UserPasswordUpdateCmdExe {

    @Resource
    private UserGateway userGateway;

    @Resource
    private UserDomainService userDomainService;

    public void execute(UserPasswordUpdateCmd cmd) {
        UserEntity user = userGateway.getOneById(StpUtil.getLoginId(""));
        if(!user.getPassword().isEqual(cmd.getPassword()))throw new GlobalException("旧密码不正确");
        user.setPassword(new PassWord(cmd.getNewPass()));
        userGateway.saveOrUpdate(user);
        //手动更新缓存
        userDomainService.reloadUserDetailCache(user.getId());
    }

}
