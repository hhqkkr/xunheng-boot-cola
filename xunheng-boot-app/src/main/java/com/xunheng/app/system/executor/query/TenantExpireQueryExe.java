package com.xunheng.app.system.executor.query;

import com.xunheng.domain.tenant.gateway.TenantGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户是否过期查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:01
 */
@Component
public class TenantExpireQueryExe {

    @Resource
    private TenantGateway tenantGateway;

    public Boolean execute(String tenantId) {
       return tenantGateway.isTenantExpire(tenantId);
    }
}
