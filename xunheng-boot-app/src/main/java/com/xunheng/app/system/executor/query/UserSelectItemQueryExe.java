package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.UserAssembler;
import com.xunheng.client.system.dto.VO.SelectItemVO;
import com.xunheng.domain.user.gateway.UserGateway;
import com.xunheng.domain.user.model.UserEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 用户选择器查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:06
 */
@Component
public class UserSelectItemQueryExe {

    @Resource
    private UserGateway userGateway;


    public List<SelectItemVO> execute(){
        List<UserEntity> list = userGateway.getSelectItem();
        return list.stream().map(UserAssembler::toSelectItem).collect(Collectors.toList());
    }


}
