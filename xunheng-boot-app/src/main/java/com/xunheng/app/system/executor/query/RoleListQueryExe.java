package com.xunheng.app.system.executor.query;

import com.xunheng.app.system.assembler.RoleAssembler;
import com.xunheng.client.system.dto.VO.RoleVO;
import com.xunheng.domain.role.gateway.RoleGateway;
import com.xunheng.domain.role.model.RoleEntity;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: xunheng-cloud-cola
 * @description: 角色列表查询处理器
 * @author: hhqkkr
 * @date: 2023/6/29 22:55
 */
@Component
public class RoleListQueryExe {

    @Resource
    RoleGateway roleGateway;

    public List<RoleVO> execute() {
        List<RoleEntity> list = roleGateway.getAll();
        return list.stream().map(RoleAssembler::toVo).collect(Collectors.toList());
    }
}
