
package com.xunheng.app.system.executor;

import com.xunheng.app.system.assembler.TenantAssembler;
import com.xunheng.client.system.dto.TenantSaveCmd;
import com.xunheng.client.system.dto.VO.TenantVO;
import com.xunheng.domain.tenant.gateway.TenantGateway;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @program: xunheng-cloud-cola
 * @description: 租户保存处理器
 * @author: hhqkkr
 * @date: 2023/6/29 23:30
 */
@Component
public class TenantSaveCmdExe {

    @Resource
    private TenantGateway tenantGateway;

    public TenantVO execute(TenantSaveCmd cmd) {
        return TenantAssembler.toVo(tenantGateway.saveOrUpdate(TenantAssembler.toSaveEntity(cmd)));
    }

}
